package com.craftcoder.sftp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.craftcoder.sftp"})
public class SftpApplication {

        public static void main(String[] args) {
        SpringApplication.run(SftpApplication.class, args);
    }

}
